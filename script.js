//-------------------------------------------Variables-------------------------------------------------
var numero1;
var numero2;
var resultadofinal;

//----------------------------------------------NUMEROS---------------------------------------------------
$(document).ready(function(){
    $("#1").on("click",function(){
    $("#resultado").append("1");
    });

    $("#2").on("click",function(){
    $("#resultado").append("2");
    });

    $("#3").on("click",function(){
    $("#resultado").append("3");
    });

    $("#4").on("click",function(){
    $("#resultado").append("4");
    });

    $("#5").on("click",function(){
    $("#resultado").append("5");
    });

    $("#6").on("click",function(){
    $("#resultado").append("6");
    });

    $("#7").on("click",function(){
    $("#resultado").append("7");
    });

    $("#8").on("click",function(){
    $("#resultado").append("8");
    });

    $("#9").on("click",function(){
    $("#resultado").append("9");
    });

    $("#0").on("click",function(){
    $("#resultado").append("0");
    });
    
    //---------------------------------------------SIMBOLOS----------------------------------------------
    $("#CE").on("click",function(){
        resetResultado();
    });

    $("#C").on("click",function(){
        resetResultado();
        });
        
    $("#suma").on("click",function(){
    numero1 = $("#resultado").html();
      resultadofinal = "+";
      limpiarTodo();
    });

    $("#resta").on("click",function(){
    numero1 = $("#resultado").html();
      resultadofinal = "-";
      limpiarTodo();
    });

    $("#multiplicacion").on("click",function(){
    numero1 = $("#resultado").html();
      resultadofinal = "*";
      limpiarTodo();
    });

    $("#division").on("click",function(){
    numero1 = $("#resultado").html();
      resultadofinal = "/";
      limpiarTodo();
    });

    $("#igual").on("click",function(){
    numero2 = $("#resultado").html();
      resultados();
    });

    $("#porcentaje").on("click",function(){
        numero2 = $("#resultado").html();
          resultados();
        });
        
   });
    //-----------------------------------------Resultados---------------------------------
   function resultados(){
    var res = 0;
    switch(resultadofinal){
    case "+":
    res = parseFloat(numero1) + parseFloat(numero2);
    break;
    
    case "-":
        res = parseFloat(numero1) - parseFloat(numero2);
        break;
    
    case "*":
    res = parseFloat(numero1) * parseFloat(numero2);
    break;
    
    case "/":
    res = parseFloat(numero1) / parseFloat(numero2);
    break;

    case "%":
        res = parseFloat(numero1) % parseFloat(numero2);
        break;
    }
    resetResultado();
    $("#resultado").html(res);
   }

   //------------------------------------Resultados finales--------------------------------------
function limpiarTodo(){
    $("#resultado").html("");
   }
    
   function resetResultado(){
    $("#resultado").html("");
    numero1 = 0;
    numero2 = 0;
    resultadofinal = "";
   }
    
